<?php
session_start();
include "helper/function.php";
include "inc/koneksi.php";

?>
<!DOCTYPE html>
<html lang="en-US" dir="ltr">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <!-- ===============================================-->
  <!--    Document Title-->
  <!-- ===============================================-->
  <title>Bina Safety Indonesia</title>


  <!-- ===============================================-->
  <!--    Favicons-->
  <!-- ===============================================-->
  <link rel="apple-touch-icon" sizes="180x180" href="frontend/public/assets/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="frontend/public/assets/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="frontend/public/assets/img/favicons/favicon-16x16.png">
  <link rel="shortcut icon" type="image/x-icon" href="frontend/public/assets/img/favicons/favicon.ico">
  <link rel="manifest" href="frontend/public/assets/img/favicons/manifest.json">
  <meta name="msapplication-TileImage" content="frontend/public/assets/img/favicons/mstile-150x150.png">
  <meta name="theme-color" content="#ffffff">


  <!-- ===============================================-->
  <!--    Stylesheets-->
  <!-- ===============================================-->
  <link href="frontend/public/assets/css/theme.css" rel="stylesheet" />

</head>


<body>

  <!-- ===============================================-->
  <!--    Main Content-->
  <!-- ===============================================-->
  <main class="main" id="top">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3 d-block" data-navbar-on-scroll="data-navbar-on-scroll">
      <div class="container"><a href="index.php">
      <img src="frontend/images/logo.png" height="45" alt="logo" />
      </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"> </span></button>
        <div class="collapse navbar-collapse border-top border-lg-0 mt-4 mt-lg-0" id="navbarSupportedContent">
          <ul class="navbar-nav ms-auto pt-2 pt-lg-0 font-base">
            <li class="nav-item px-2"><a class="nav-link" aria-current="page" href="index.php">Beranda</a></li>
            <li class="nav-item px-2"><a class="nav-link" href="index.php#services">Menu</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- <section class="py-xxl-10 pb-0" id="home">
      <div class="bg-holder bg-size" style="background-image:url(public/assets/img/gallery/hero-header-bg.png);background-position:top center;background-size:cover;">
      </div>

      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-5 col-xl-6 col-xxl-7 order-0 order-md-1 text-end"><img class="pt-7 pt-md-0 w-100" src="images/index.png" alt="hero-header" /></div>
          <div class="col-md-75 col-xl-6 col-xxl-5 text-md-start text-center py-8">
            <h1 class="fw-normal fs-6 fs-xxl-7">BSI </h1>
            <h1 class="fw-bolder fs-6 fs-xxl-7 mb-2">"Bina Safety Indonesia"</h1>

          </div>
        </div>
      </div>
    </section> -->


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="py-9" id="services" container-xl="container-xl">

      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-8 col-lg-5 text-center mb-1">
            <h4 class="text-danger">Cari Personel SIO</h4>
          </div>
        </div>
        <div class="row h-100 justify-content-center">
          <div class="col-md-7 pt-2 px-md-2 px-lg-5">
            <div class="card h-100 px-lg-5 card-span">
              <div class="card-body d-flex flex-column justify-content-around">
                <div class="text-center pt-3">
                  <div class="row">
                    <form action="" method="POST" class="form-check-label">
                      <div class="col-md-12">
                        <label for="">Nama</label>
                      </div>
                      <div class="col-md-12">
                        <input type="text" name="nama" id="" class="form-control" required>
                      </div>
                      <div class="col-md-12">
                        <label for="ttl">Tanggal Lahir</label>

                      </div>
                      <div class="col-md-12">
                        <input type="date" name="tgl_lahir" id="ttl" class="form-control" required>

                      </div>
                  </div>
                </div>
                <div class="text-center my-2">
                  <div class="d-grid">
                    <button class="btn btn-outline-danger" name="btnCari" type="submit">Cari</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end of .container-->

    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->
    <script type='text/javascript'> 
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
    <?php
    if (isset($_POST['btnCari'])) {
      $nama = mysqli_real_escape_string($koneksi, $_POST['nama']);
      $tgl_lahir = mysqli_real_escape_string($koneksi, $_POST['tgl_lahir']);
      $param = array("nama" => $nama, "tgl_lahir" => $tgl_lahir, "pekerjaan_id" => 2);
      $res = request("api_pekerja.php?f=get_pekerja&" . http_build_query($param));
      $jumlah_cari = count($res->pekerja);



      if ($jumlah_cari >= 1) {
        $data_cari = (array)$res->pekerja[0];
        $_SESSION["ses_nama_pekerja"] = $data_cari["nama"];
        $_SESSION["ses_tgl_lahir"] = $data_cari["tgl_lahir"];
        $_SESSION["ses_operator"] = $data_cari["operator"];

        echo "<script>
                Swal.fire({title: 'Data Ditemukan',text: '',icon: 'success',confirmButtonText: 'OK'
                }).then((result) => {
                    if (result.value) {
                        window.location = 'hasil_sio.php#services';
                    }
                })</script>";
      } else {
        echo "<script>
                Swal.fire({title: 'Data Tidak Ditemukan',text: '',icon: 'error',confirmButtonText: 'OK'
                }).then((result) => {
                    if (result.value) {
                        window.location = '#services';
                    }
                })</script>";
      }
    }
    ?>









    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="py-0 bg-1000" style="position:fixed;bottom:0;width:100%">

      <div class="container">
        <div class="row justify-content-md-between justify-content-evenly py-4">
          <div class="col-12 col-sm-8 col-md-6 col-lg-auto text-center text-md-start">
            <p class="fs--1 my-2 fw-bold text-200">All rights Reserved &copy; Bina Safety, 2021</p>
          </div>
        </div>
      </div>
      <!-- end of .container-->

    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


  </main>
  <!-- ===============================================-->
  <!--    End of Main Content-->
  <!-- ===============================================-->




  <!-- ===============================================-->
  <!--    JavaScripts-->
  <!-- ===============================================-->
  <script src="frontend/public/vendors/@popperjs/popper.min.js"></script>
  <script src="frontend/public/vendors/bootstrap/bootstrap.min.js"></script>
  <script src="frontend/public/vendors/is/is.min.js"></script>
  <script src="frontend/public/https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
  <script src="frontend/public/vendors/fontawesome/all.min.js"></script>
  <script src="frontend/public/assets/js/theme.js"></script>

  <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">

  <!-- jQuery 2.2.3 -->
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="bootstrap/js/bootstrap.min.js"></script>

  <script src="plugins/select2/select2.full.min.js"></script>
  <!-- DataTables -->
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

  <!-- AdminLTE App -->
  <script src="dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <!-- page script -->


  <script>
    $(function() {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>

  <script>
    $(function() {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
  </script>
</body>


</html>