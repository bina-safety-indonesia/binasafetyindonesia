<?php
	$sql = $koneksi->query("SELECT count(id_pekerja) as pekerja from tb_pekerja");
	while ($data= $sql->fetch_assoc()) {
	
		$pekerja=$data['pekerja'];
	}
?>

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dashboard
		<small>Umum</small>
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">

		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-blue">
				<div class="inner">
					<h4>
						<?= $pekerja; ?>
					</h4>
					<p>Ahli K3</p>
				</div>
				<div class="icon">
					<i class="ion ion-stats-bars"></i>
				</div>
				<a href="?page=MyApp/ahli_k3" class="small-box-footer">Cari Data
					<i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>

		