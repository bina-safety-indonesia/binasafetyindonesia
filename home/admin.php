<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dashboard
		<small>Administrator</small>
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		
		<a href="?page=MyApp/data_ahlik3" class="small-box">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-blue">
				<div class="inner">
				<h4>
						<?= "Data"; ?>
					</h4>
					<p>Ahli K3</p>
				</div>
				<div class="icon">
					<i class="ion ion-stats-bars"></i>
				</div>
					<!-- <i class="fa fa-arrow-circle-right"></i> -->
			</div>
		</div>
		</a>
	</div>
	<div class="row">
		
		<a href="?page=MyApp/data_sio" class="small-box">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-blue">
				<div class="inner">
				<h4>
						<?= "Data"; ?>
					</h4>
					<p>Surat Izin Operator</p>
				</div>
				<div class="icon">
					<i class="ion ion-stats-bars"></i>
				</div>
					<!-- <i class="fa fa-arrow-circle-right"></i> -->
			</div>
		</div>
		</a>
	</div>
	<div class="row">
		
		<a href="?page=MyApp/data_welder" class="small-box">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-blue">
				<div class="inner">
				<h4>
						<?= "Data"; ?>
					</h4>
					<p>Welder</p>
				</div>
				<div class="icon">
					<i class="ion ion-stats-bars"></i>
				</div>
					<!-- <i class="fa fa-arrow-circle-right"></i> -->
			</div>
		</div>
		</a>
	</div>
</section>
		
