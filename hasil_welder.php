<?php 
    session_start();
    include "helper/function.php";
    include "inc/koneksi.php";
    $data_nama_pekerja = $_SESSION["ses_nama_pekerja"];
    $data_tgl_lahir = $_SESSION["ses_tgl_lahir"];
    $data_operator = $_SESSION["ses_operator"];
?>
<!DOCTYPE html>
<html lang="en-US" dir="ltr">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>Bina Safety Indonesia</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="frontend/public/assets/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="frontend/public/assets/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="frontend/public/assets/img/favicons/favicon-16x16.png">
    <link rel="shortcut icon" type="image/x-icon" href="frontend/public/assets/img/favicons/favicon.ico">
    <link rel="manifest" href="frontend/public/assets/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="frontend/public/assets/img/favicons/mstile-150x150.png">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="frontend/public/assets/css/theme.css" rel="stylesheet" />

  </head>


  <body>

    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main" id="top">
      <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3 d-block" data-navbar-on-scroll="data-navbar-on-scroll">
        <div class="container">
        <a href="index.php">
        <img src="frontend/images/logo.png" height="45" alt="logo" />
        </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"> </span></button>
          <div class="collapse navbar-collapse border-top border-lg-0 mt-4 mt-lg-0" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto pt-2 pt-lg-0 font-base">
              <li class="nav-item px-2"><a class="nav-link" aria-current="page" href="index.php">Beranda</a></li>
              <li class="nav-item px-2"><a class="nav-link" aria-current="page" href="index.php#services">Menu</a></li>
              
        
            </ul>
          </div>
        </div>
      </nav>
     


      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-9" id="services" container-xl="container-xl">

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-5 text-center mb-1">
               <img src="frontend/images/logo.png" height="75" alt="logo" />
              <h4 class="text-danger">Data WELDER</h4>
            </div>
          </div>
          <div class="row h-100 justify-content-center">
            <div class="col-md-8 pt-2 px-md-2 px-lg-8">
              <div class="card h-100 px-lg-5 card-span">
                <div class="card-body d-flex flex-column justify-content-around">
                  <!--<div class="text-center pt-1">-->
                    <!--<h5 class="my-2">Ahli K3</h5>-->
                    <!--<br>-->
                  <!--</div>-->
                  <div class="row text-center">
                      <table>
                      <div class="col-md-4">
                                <th>Nama</th>
                            </div>
                            <div class="col-md-4">
                                <th>Pekerjaan</th>
                            </div>
                            <div class="col-md-4">
                                <th>Tanggal Lahir</th>
                            </div>
                        </div>
                        <tr>
                            <td><?php echo $data_nama_pekerja ?></td>
                            <td><?php echo $data_operator ?></td>
                            <td><?php echo $data_tgl_lahir ?></td>
                        </tr>
                    </table>
                    </div>
                  <div class="text-center my-5">
                   <div class="d-grid">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->





     




      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0 bg-1000"  style="position:fixed;bottom:0;width:100%">

        <div class="container">
          <div class="row justify-content-md-between justify-content-evenly py-4">
            <div class="col-12 col-sm-8 col-md-6 col-lg-auto text-center text-md-start">
              <p class="fs--1 my-2 fw-bold text-200">All rights Reserved &copy; Bina Safety Indonesia, 2021</p>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->


    </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="frontend/public/vendors/@popperjs/popper.min.js"></script>
    <script src="frontend/public/vendors/bootstrap/bootstrap.min.js"></script>
    <script src="frontend/public/vendors/is/is.min.js"></script>
    <script src="frontend/public/https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
    <script src="frontend/public/vendors/fontawesome/all.min.js"></script>
    <script src="frontend/public/assets/js/theme.js"></script>

    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">
  </body>

</html>