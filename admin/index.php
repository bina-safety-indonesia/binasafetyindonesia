<?php
//Mulai Sesion
session_start();
if (isset($_SESSION["ses_username"]) == "") {
	header("location: login.php");
} else {
	$timeout = 5; // setting timeout dalam menit
 	$logout = "login.php"; // redirect halaman logout
 
 	$timeout = $timeout * 60; // menit ke detik
 	if(isset($_SESSION['start_session'])){
 	$elapsed_time = time()-$_SESSION['start_session'];
 	if($elapsed_time >= $timeout){
 	session_destroy();
 	echo "<script type='text/javascript'>alert('Sesi telah berakhir, silahkan login ulang');window.location='$logout'</script>";
 	}
 	}
 	$_SESSION['start_session']=time();
	$data_id = $_SESSION["ses_id"];
	$data_nama = $_SESSION["ses_nama"];
	$data_user = $_SESSION["ses_username"];
	$data_level = $_SESSION["ses_level"];
}


//KONEKSI DB
include "../inc/koneksi.php";
include "../helper/function.php";
$data_pekerjaan = request("/api_pekerjaan.php?f=get_pekerjaan");
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BinaSafetyIndonesia</title>
	<link rel="icon" href="../dist/img/BSI.png">
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="../plugins/select2/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<body class="hold-transition skin-green sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="index.php" class="logo">
				<span class="logo-lg">
					<img src="../dist/img/BSI.png" width="70px">
					<b>BSI</b>
				</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<li class="dropdown messages-menu">
							<a class="dropdown-toggle">
								<span>
									<b>
										PT. Bina Safety Indonesia (BSI)
									</b>
								</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<b>
					<div class="user-panel">
						<div class="pull-left image">
							<img src="../dist/img/avatar2.png" class="img-circle" alt="User Image">
						</div>
						<div class="pull-left info">
							<p>
								<?php echo $data_nama; ?>
							</p>
							<span class="label label-warning">
								<?php echo $data_level; ?>
							</span>
						</div>
					</div>
					</br>
					<!-- /.search form -->
					<!-- sidebar menu: : style can be found in sidebar.less -->
					<ul class="sidebar-menu">
						<li class="header">MAIN NAVIGATION</li>

						<!-- Level  -->
						<?php
						if ($data_level == "Administrator") {
						?>

							<li class="treeview">
								<a href="?page=admin">
									<i class="fa fa-dashboard"></i>
									<span>Dashboard</span>
									<span class="pull-right-container">
									</span>
								</a>
							</li>

							<?php foreach ($data_pekerjaan->pekerjaan as $p) : ?>
								<li class="treeview">
									<a href="<?= $p->link ?>">
										<i class="<?= $p->icon ?>"></i>
										<span><?= $p->nama_pekerjaan ?></span>
										<span class="pull-right-container">
										</span>
									</a>
								</li>
							<?php endforeach ?>


							<li class="header">SETTING</li>

							<li class="treeview">
								<a href="?page=MyApp/data_pengguna">
									<i class="fa fa-user"></i>
									<span>Pengguna Sistem</span>
									<span class="pull-right-container">
									</span>
								</a>
							</li>

							<li>
								<a href="logout.php" onclick="return confirm('Anda yakin untuk logout ?')">
									<i class="fa fa-sign-out"></i>
									<span>Logout</span>
									<span class="pull-right-container"></span>
								</a>
							</li>

						<?php
						}
						?>

			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- =============================================== -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<!-- Main content -->
			<section class="content">
				<?php
				if (isset($_GET['page'])) {
					$hal = $_GET['page'];

					switch ($hal) {
							//Klik Halaman Home Pengguna
						case 'admin':
							include "../home/admin.php";
							break;

							//Pengguna
						case 'MyApp/data_pengguna':
							include "pengguna/data_pengguna.php";
							break;
						case 'MyApp/add_pengguna':
							include "pengguna/add_pengguna.php";
							break;
						case 'MyApp/edit_pengguna':
							include "pengguna/edit_pengguna.php";
							break;
						case 'MyApp/del_pengguna':
							include "pengguna/del_pengguna.php";
							break;

							//pekerja
						case 'MyApp/data_ahlik3':
							include "pekerja/ahlik3/data_pekerja.php";
							break;
						case 'MyApp/add_ahlik3':
							include "pekerja/ahlik3/add_pekerja.php";
							break;
						case 'MyApp/edit_ahlik3':
							include "pekerja/ahlik3/edit_pekerja.php";
							break;
						case 'MyApp/del_ahlik3':
							include "pekerja/ahlik3/del_pekerja.php";
							break;
						case 'MyApp/data_sio':
							include "pekerja/sio/data_pekerja.php";
							break;
						case 'MyApp/add_sio':
							include "pekerja/sio/add_pekerja.php";
							break;
						case 'MyApp/edit_sio':
							include "pekerja/sio/edit_pekerja.php";
							break;
						case 'MyApp/del_sio':
							include "pekerja/sio/del_pekerja.php";
							break;
						case 'MyApp/data_welder':
							include "pekerja/welder/data_pekerja.php";
							break;
						case 'MyApp/add_welder':
							include "pekerja/welder/add_pekerja.php";
							break;
						case 'MyApp/edit_welder':
							include "pekerja/welder/edit_pekerja.php";
							break;
						case 'MyApp/del_welder':
							include "pekerja/welder/del_pekerja.php";
							break;

							//default
						default:
							echo "<center><br><br><br><br><br><br><br><br><br>
				  <h1> Halaman tidak ditemukan !</h1></center>";
							break;
					}
				} else {
					// Auto Halaman Home Pengguna
					if ($data_level == "Administrator") {
						include "../home/admin.php";
					} else {
						include "../home/login.php";
					}
				}
				?>



			</section>
			<!-- /.content -->
		</div>

		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
			</div>
			<strong>Copyright &copy;
				<a href="https://binasafetyindonesia.id">Bina Safety Indonesia</a>.</strong> 2021.
		</footer>
		<div class="control-sidebar-bg"></div>

		<!-- ./wrapper -->

		<!-- jQuery 2.2.3 -->
		<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="../bootstrap/js/bootstrap.min.js"></script>

		<script src="../plugins/select2/select2.full.min.js"></script>
		<!-- DataTables -->
		<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>

		<!-- AdminLTE App -->
		<script src="../dist/js/app.min.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="../dist/js/demo.js"></script>
		<!-- page script -->


		<script>
			$(function() {
				$("#example1").DataTable();
				$('#example2').DataTable({
					"paging": true,
					"lengthChange": false,
					"searching": false,
					"ordering": true,
					"info": true,
					"autoWidth": false
				});
			});
		</script>

		<script>
			$(function() {
				//Initialize Select2 Elements
				$(".select2").select2();
			});
		</script>
</body>

</html>
