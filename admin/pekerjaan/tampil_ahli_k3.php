<?php 
    $data_nama_pekerja = $_SESSION["ses_nama_pekerja"];
    $data_tgl_lahir = $_SESSION["ses_tgl_lahir"];
    $data_operator = $_SESSION["ses_operator"];
?>

<section class="content-header">
	<h1>
		Ahli K3
		<small>Hasil Pencarian</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>
				<b>Bina Safety Indonesia</b>
			</a>
		</li>
	</ol>
</section>
<section>
    <div class="box-body">
			<div class="table-responsive">
				<table id="example1" class="table table-bordered table-striped">
                    <tr>
                        <td>Nama  </td>
                        <td><?php echo $data_nama_pekerja ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Lahir  </td>
                        <td><?php echo $data_tgl_lahir ?></td>
                    </tr>
                    <tr>
                        <td> Operator  </td>
                        <td> <?php echo $data_operator ?></td>
                    </tr>
                </table>
            </div>
    </div>
</section>