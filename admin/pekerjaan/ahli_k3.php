
<section class="content-header">
	<h1>
		Ahli K3
		<small>Pencarian Data</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>
				<b>Bina Safety Indonesia</b>
			</a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Cari Data</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove">
							<i class="fa fa-remove"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="#" method="post">
				<div class="form-group has-feedback">
					<input type="text" class="form-control" name="nama" placeholder="Nama" required>
				</div>
				<div class="form-group has-feedback">
					<input type="date" class="form-control" name="tgl_lahir" placeholder="Tanggal Lahir" required>
				</div>
				<div class="row">
					<div class="col-xs-8">

					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" class="btn btn-success btn-block btn-flat" name="btnCari" title="Cari Data">
							<b>Cari</b>
						</button>
					</div>
			<!-- /.box -->
</section>

<?php 
		if (isset($_POST['btnCari'])) {  
			
			$nama=mysqli_real_escape_string($koneksi,$_POST['nama']);
			$tgl_lahir=mysqli_real_escape_string($koneksi,$_POST['tgl_lahir']);
			$param = array("nama"=>$nama, "tgl_lahir"=>$tgl_lahir, "pekerjaan_id"=>1);
			$res = request("api_pekerja.php?f=get_pekerja&".http_build_query($param));
			$jumlah_cari = count($res->pekerja);
		
        

            if ($jumlah_cari >= 1 ){
				$data_cari = (array)$res->pekerja[0];
              $_SESSION["ses_nama_pekerja"]=$data_cari["nama"];
              $_SESSION["ses_tgl_lahir"]=$data_cari["tgl_lahir"];
              $_SESSION["ses_operator"]=$data_cari["operator"];
                
              echo "<script>
                    Swal.fire({title: 'Data Ditemukan',text: '',icon: 'success',confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.value) {
                            window.location = '?page=MyApp/tampil_ahli_k3';
                        }
                    })</script>";
              }else{
              echo "<script>
                    Swal.fire({title: 'Data Tidak Ditemukan',text: '',icon: 'error',confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.value) {
                            window.location = '#';
                        }
                    })</script>";
                }
			  }
