<?php
//kode 9 digit
// echo $_SERVER["DOCUMENT_ROOT"];
// die;
// include $_SERVER["DOCUMENT_ROOT"]."/binasafetyindonesia/helper/function.php";

$data_pekerjaan = request("../api_pekerjaan.php?f=get_pekerjaan");
					
								
$carikode = mysqli_query($koneksi,"SELECT id_pekerja FROM tb_pekerja order by id_pekerja desc");

$datakode = mysqli_fetch_array($carikode);
$kode = $datakode['id_pekerja'];
$urut = substr($kode, 1, 3);
$tambah = (int) $urut + 1;
?>
<section class="content-header">
	<h1>
		Kelola Data
		<small>Data Ahli K3</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="../index.php">
				<i class="fa fa-home"></i>
				<b>Bina Safety Indonesia</b>
			</a>
		</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Tambah Pekerja</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove">
							<i class="fa fa-remove"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="" method="post" enctype="multipart/form-data">
					<div class="box-body">
	

						<div class="form-group">
							<label>Nama</label>
							<input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Pekerja" required>
						</div>

						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input type='date' name="tgl_lahir" id="tgl_lahir" class="form-control" required>
						</div>

						<div class="form-group">
							<label>Pekerjaan</label>
							<input type="text" name="operator" id="operator" class="form-control" placeholder="operator" required>
						</div>

						<div class="form-group">
							<!-- <label>Keterangan</label> -->
							<input type="hidden" name="keterangan" id="keterangan" class="form-control" readonly value="1" placeholder="Ahli K3">
						</div>

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<input type="submit" name="Simpan" value="Simpan" class="btn btn-info">
						<a href="?page=MyApp/data_ahlik3" class="btn btn-warning">Batal</a>
					</div>
				</form>
			</div>
			<!-- /.box -->
</section>

<?php

    if (isset ($_POST['Simpan'])){
    
        $sql_simpan = "INSERT INTO tb_pekerja (nama,tgl_lahir,operator,pekerjaan_id) VALUES (
          '".$_POST['nama']."',
          '".$_POST['tgl_lahir']."',
          '".$_POST['operator']."',
          '".$_POST['keterangan']."')";
        $query_simpan = mysqli_query($koneksi, $sql_simpan);
        mysqli_close($koneksi);

    if ($query_simpan){

      echo "<script>
      Swal.fire({title: 'Tambah Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
      }).then((result) => {
          if (result.value) {
              window.location = 'index.php?page=MyApp/data_ahlik3';
          }
      })</script>";
      }else{
      echo "<script>
      Swal.fire({title: 'Tambah Data Gagal',text: '',icon: 'error',confirmButtonText: 'OK'
      }).then((result) => {
          if (result.value) {
              window.location = 'index.php?page=MyApp/add_ahlik3';
          }
      })</script>";
    }
  }
    
