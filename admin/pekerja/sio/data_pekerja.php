<?php
    $sql = $koneksi->query("SELECT tp.*,tpn.nama_pekerjaan from tb_pekerja tp join tb_pekerjaan tpn on tp.pekerjaan_id=tpn.id_pekerjaan where  tp.pekerjaan_id = 2");
	$d = $sql->fetch_row();

?>
<section class="content-header">
	<h1>
		Kelola Data
		<small><?= $d[5]; ?></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="../index.php">
				<i class="fa fa-home"></i>
				<b>Bina Safety Indonesia</b>
			</a>
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<a href="?page=MyApp/add_sio" title="Tambah Data" class="btn btn-primary">
				<i class="glyphicon glyphicon-plus"></i> Tambah Data</a>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="table-responsive">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Tgl Lahir</th>
							<th>Operator</th>
							<th>Keterangan</th>
							<th>Kelola</th>
						</tr>
					</thead>
					<tbody>

						<?php
                  $no = 1;
                  while ($data= $sql->fetch_assoc()) {
                ?>

						<tr>
							<td>
								<?php echo $no++; ?>
							</td>
							<td>
								<?php echo $data['nama']; ?>
							</td>
							<td>
								<?php echo $data['tgl_lahir']; ?>
							</td>
							<td>
								<?php echo $data['operator']; ?>
							</td>
							<td>
								<?php echo $data['nama_pekerjaan']; ?>
							</td>

							<td>
								<a href="?page=MyApp/edit_sio&kode=<?php echo $data['id_pekerja']; ?>" title="Ubah"
								 class="btn btn-success">
									<i class="glyphicon glyphicon-edit"></i>
								</a>
								<a href="?page=MyApp/del_sio&kode=<?php echo $data['id_pekerja']; ?>" onclick="return confirm('Yakin Hapus Data Ini ?')"
								 title="Hapus" class="btn btn-danger">
									<i class="glyphicon glyphicon-trash"></i>
							</td>
						</tr>
						<?php
                  }
                ?>
					</tbody>

				</table>
			</div>
		</div>
	</div>
</section>